from celery import Celery

celery_app = Celery("worker", broker="redis://redis/0")

celery_app.conf.task_routes = {"neoh_backend.worker.test_celery": "main-queue"}
